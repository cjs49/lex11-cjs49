
all =  Bind.o BindList.o BindListList.o Expr.o \
            ExprList.o Primitives.o REPL.o interpreter.o interpreter.c  

objects = (*.o)


nterpreter : $(all)
	gcc -M -o interpreter $(all)	     



interpreter.o : interpreter.c

Bind.o : defs.h Bind.c

BindList.o : defs.h BindList.c Bind.c

BindListList.o : defs.h BindListList.c BindList.c Bind.c

Expr.o : defs.h Expr.c

ExprList.o : defs.h ExprList.c Expr.c

Primitives.o : defs.h Primitives.c

REPL.o : defs.h REPL.c 

all : |$(all)

$(all):
	mkdir $(all) 	
	

.PHONY: clean
clean :
	rm *.o 

